<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Poll;
use Faker\Generator as Faker;

$factory->define(Poll::class, function (Faker $faker) {
    return [
        'poll_description' => $faker->sentence,
        'views' => $faker->randomNumber(5)
    ];
});
