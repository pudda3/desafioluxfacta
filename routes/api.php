<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/poll/{id}', 'Poll\PollController@index')->name('polls');
Route::get('/poll/{id}/stats', 'Poll\PollController@stats')->name('poll.stats');

Route::post('/poll', 'Poll\PollController@poll')->name('poll.poll');
Route::post('/poll/{id}/vote', 'Poll\PollController@vote')->name('poll.vote');
