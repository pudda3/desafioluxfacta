## Desafio

Solução do desafio composta por Lucas Nunes Joaquim.

### Requisitos
- Composer 1.7+
- MySQL 5.7+
- PHP 7.1+  
    Com as extensões:
    - PDO PHP Extension
    - OpenSSL PHP Extension
    - Mbstring PHP Extension
    - JSON PHP Extension

## Passos para Deploy

1. Extrair o arquivo zip para uma pasta **acima** da pasta `public_html`.  
    Ex:

    ```
    - /
       pasta_projeto/
       public_html/
       www/
      . . .
    ```
2. Entre na pasta do projeto e altere o arquivo `.env` com as informações do bando de dados:

    ```
    DB_CONNECTION=mysql 
    DB_HOST=localhost       // Endereco do servidor de banco
    DB_PORT=3306            // Porta do servidor
    DB_DATABASE=db_name     // Nome do banco de dados
    DB_USERNAME=db_user     // Usuario de acesso ao banco de dados
    DB_PASSWORD=db_password // Senha do usuario do banco
    ```

3. Entre na pasta do projeto e execute os comandos:
    ```bash 
        $ composer install --no-dev

        $ artisan config:cache

        $ artisan route:cache
    ```

4. Após executar os comandos, copie todo o conteudo da pasta `pasta_projeto/public/` para a pasta `public_html`.

5. Edite o arquivo `index.php` que foi copiado conforme o sequinte:
    - Localize os seguintes trechos:
        ```php
            . . .

            require __DIR__.'/../vendor/autoload.php';

            . . .

            $app = require_once __DIR__.'/../bootstrap/app.php';

        ```
    - Altere o caminho para pegar os arquivos da pasta onde esta o projeto:
        ```php
            . . .

            require __DIR__.'/../pasta_projeto/vendor/autoload.php';

            . . .

            $app = require_once __DIR__.'/../pasta_projeto/bootstrap/app.php';

        ```
6. Agora está tudo certo

## End Points

### GET /poll/:id

Retorno as informações da poll que tiver o `poll_id` igual ao valor informado no parametro `:id`.

### POST /poll

Recebe um objeto poll como parametro e persiste no banco de dados e retorna o poll_id do objeto persistido.  
Formato do parametro:

```json
{
    "poll_description": "This is the question",
    "options": [
        "First Option",
        "Second Option",
        "Third Option"
    ]
}
```

Formado do retorno:

```json
{
    "poll_id": 10
}
```

### POST /poll/:id/vote
Registra um voto para uma opção da poll enviada no parametro `:id`. Recebe o `option_id` como parametro para registrar o voto.  
Formato de parametro:
```json
{
    "option_id": 1
}
```

### GET /poll/:id/stats

Retorna as estatísticas da poll, quantas views a poll tem e quantos votos as opções tiveram.  
Formato de resposta:

```json
{
    "views": 125,
    "votes": [
        {"option_id": 1, "qty": 10},
        {"option_id": 2, "qty": 35},
        {"option_id": 3, "qty": 1},
    ]
}
```
