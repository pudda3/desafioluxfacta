<?php

namespace App\Http\Controllers\Poll;

use App\Poll;
use App\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PollController extends Controller
{
    public function index($id)
    {

        $poll = Poll::select('poll_id', 'poll_description')
            ->with(
                array(
                    'options' => function($query){
                        $query->select(['option_id','option_description', 'poll_id']);
                    }
                )
            )
            ->where('poll_id', $id)
            ->first();

        if($poll){

            Poll::where('poll_id', $id)
                ->increment('views');

            foreach ($poll['options'] as $key => $value) {
                unset($value['poll_id']);
            }

            return response()->json($poll, 200);

        } else {
            return response()->json(['error' => 'Poll not found!'], 404);
        }

    }

    public function stats($id)
    {
        $stats = [];

        $poll = Poll::where('poll_id', $id)->first();

        if($poll){

            $stats['views'] = $poll->views;
            $stats['votes'] = Option::select('option_id', 'votes as qty')->where('poll_id', $id)->get();
            
            return response()->json($stats, 200);

        } else {
            return response()->json(['error' => 'Poll not found!'], 404);
        }

    }

    public function vote(Request $request, $id)
    {
        if(count($request->all())<=0) return response()->json(['error' => 'Request params are empty!'], 400);
        
        $allOptions = Option::where('poll_id', $id)->get();
        
        if(!$allOptions->isEmpty()){

            $optionSelected = Option::where('poll_id', $id)->where('option_id',$request->option_id)->get();

            if(!$optionSelected->isEmpty()){

                Option::where('poll_id', $id)->where('option_id',$request->option_id)->increment('votes');

                return response()->json(['sucess' => 'Vote computed!'], 200);
                
            } else {
                return response()->json(['error' => 'Option not found!'], 404);
            }
            
        } else {
            return response()->json(['error' => 'Poll not found!'], 404);
        }
    }

    public function poll(Request $request)
    {
        if(count($request->all())<=0) return response()->json(['error' => 'Request params are empty!'], 400);

        if(!empty($request->poll_description)){
            $pollData = [
                'poll_description' => $request->poll_description,
                'views' => 0
            ];
        } else {
            return response()->json(['error' => 'Param poll_description is empty!'], 400);
        }
        
        if(!empty($request->options)){
            $optionsData = [];

            foreach ($request->options as $key => $value) {
                $optionsData[] = [
                    'option_description' => $value,
                    'votes' => 0
                ];
            }
        } else {
            return response()->json(['error' => 'Param options is empty!'], 400);
        }
        
        $poll = Poll::create($pollData);
        $poll->options()->createMany($optionsData);
        return response()->json(['poll_id' => $poll->poll_id], 201);

        
    }
}
