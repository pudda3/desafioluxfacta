<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    //
    protected $table = 'polls';
    protected $primaryKey = 'poll_id';

    public $fillable = [
        'poll_description',
        'views'
    ];

    public function options()
    {
        return $this->hasMany('App\Option', 'poll_id', 'poll_id');
    }
}
