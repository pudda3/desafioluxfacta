<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    //
    public $timestamps = false;
    public $fillable = [
        'option_description',
        'votes'
    ];

    public function polls()
    {
        return $this->belongsTo('App\Poll');
    }
}
